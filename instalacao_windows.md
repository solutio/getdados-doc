# GetDados - Instalação em ambiente windows

Para instalar o agente getDados em ambiente Windows é necessário baixá-lo, através do link informado pelo time de suporte. Logo após sigas os passos:

## 1. Descompacte

O primeiro passo é descompactar. Procure fazer isto em uma pasta vazia e definitiva, para que você possa lembrar depois.

![Descompacte](/images/descompactar.png)

## 2. Execute o arquivo install.bat

Localize o arquivo **install.bat** 

![Clique em install.bat](/images/install.bat.png)

E execute como **Administrador**

![Clique em install.bat](/images/executa_install.bat.png)

No final, aparecerá esta tela

![Clique em install.bat](/images/final_instalacao.png)

Digite qualquer tecla. 


## 3. Confirme se a instalação foi bem sucedida

Após instalado, o getDados aparece como serviço na tela abaixo.

![Clique em install.bat](/images/services.msc.png)

Para apresentar esta tela, basta executar **services.msc**, conforme abaixo:

![Clique em install.bat](/images/exec_services.msc.png)


## 4. Configuração

Após instalado, acesse o link: http://localhost:30000/  e verá a seguinte tela:

![Clique em install.bat](/images/config_getdados.png)

E preencha com os dados de conexão com o banco de dados. Clique em **Salvar e configurar**.


## 5. Pronto!

Agora os dados começam s integrar com o servidor.


![Clique em install.bat](/images/chucknorris.jpg)