# GetDados

## Introdução

O projeto **GetDados** possibilita a integração de dados entre redes e servidores. 

Por exemplo: Imagine um computador dentro de uma loja que tenha os dados de vendas daquela loja. Havendo a necessidade de integrar dados desta loja com algum outro servidor em nuvem, o projeto **GetDados** pode ser uma opção.

Exemplo de aplicações: consolidação de vendas de lojas em um servidor da matriz. Conciliação de dados entre loja e matriz. etc...

A Figura a seguir ilustra o funcionamento do **GetDados**.

![Diagrama GetDados](/images/getdados.png)

A Figura ilustra o **GetDados** como um agente que pode ser executado em um computador que acesse os dados que deseja-se integrar. Após sua execução ele é capaz de ler os dados da estrutura do cliente e enviar para o repositório em nuvem desejado. Na Figura, este repositório está identificado como **Repositório remoto de dados**.
Este repositório pode:

1. Ser do próprio cliente. Exemplo: Um repositório de consolidação de vendas.
1. Ser de algum fornecedor de serviços. Exemplo: Um repositório de conciliação de vendas em cartões que precisa receber dados. Ou um repositório do fornecedor de serviço de cashback.
1. Ser utilizado para BI e Dashboards. Exemplo: Integração de dados para apresentação de relatórios.

A Figura iustra também o serviço do **Khala**. Este é um serviço do próprio **GetDados** que fica em nuvem. Seu objetivo é o de permitir:

1. Monitoramento dos agentes **GetDados**. Através do **Khala** é possível identificar agentes que pararam de comunicar e monitorar a disponibilidade.
1. Atualizar rotinas que devem ser executadas no ambiente local do cliente (onde o agente está instalado).

## Implementação do código de integração

A Figura a seguir ilustra a relação entre o **GetDados** e o seu código de integração.

![Diagrama GetDados](/images/getdados-especifico.png)

Observe que internamente ao agente existe um código que contém os scripts de integração, SQLs, rotas e endpoints. Este código é enviado ao **GetDados** através de um sistema chamado **Khala**. Desta forma torna-se possível e bastante facilitado a atualização de modificações, correções quando esta estão relacionadas ao código de integração específico.

O código de integração pode ser implementado utilizando as seguintes tecnologias:

1. Xml MyBatis (https://mybatis.org/mybatis-3/sqlmap-xml.html)
1. Rotas Camel (https://camel.apache.org/components/latest/spring.html)
1. JavaScript

A seguir exemplos de como estas tecnologias interagem dentro do agente **GetDados**

### Exemplo de uma estrutura xml do MyBatis:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="Mensagem">
	<resultMap id="MensagemResult" type="Map">
		<result property="id" column="ID"/>
		<result property="descricao" column="DESC"/>
		<result property="dataMsg" column="DT_MENSAGEM"/>
	</resultMap>

	<select id="selectMensagem" resultMap="MensagemResult">
		select ID, DESC, DT_MENSAGEM from TBL_MENSAGEM 	where LIDA = false;
	</select>
	<update id="consumeMensagem" parameterType="Map">
		update TBL_MENSAGEM set LIDA = true where ID = #{id}
	</update>
</mapper>
```

Neste exemplo existem duas instruções SQL: um select, que foi chamado de **selectMensagem** e um update que foi chamado de **consumeMensagem**. O objetivo de **selectMensagem** é listar todas as mensagens que possuem o atributo *LIDA* como false.
Já o objetivo de **consumeMensagem** é atualizar o atributo *LIDA* para **true** de uma mensagem específica.

O **mapper** **MensagemResult** realiza o mapeamento do resultado do SQL em um mapa. Observe que ele também é capaz de trocar o nome das colunas para nomes mais apropriados para a integração, ou seja, realizar o mapeamento de dados. Por exemplo: a coluna *DT_MENSAGEM* do resultado do SQL será retornada como dataMsg no mapa.



### Exemplo de uma estrutura xml das rotas do Camel:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="
       http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
       http://camel.apache.org/schema/spring http://camel.apache.org/schema/spring/camel-spring.xsd">

   <bean class="com.solutioin.camel.dbmapper.SpringContextUtil" />
   <bean id="dataSourceGetDados" class="org.springframework.jdbc.datasource.DriverManagerDataSource">
      <property name="driverClassName" value="${getDados.database.driver}" />
      <property name="url" value="${getDados.database.url}" />
      <property name="username" value="${getDados.database.user}" />
      <property name="password" value="${getDados.database.pass}" />
   </bean>
   
    <bean id="jms-ex"
        class="org.apache.activemq.camel.component.ActiveMQComponent">
        <property name="connectionFactory">
            <bean class="org.apache.activemq.ActiveMQConnectionFactory">
                <property name="brokerURL" value="${getDados.queue.url}" />
            </bean>
        </property>
    </bean>
    
    <bean id="mapProcessor" class="com.solutioin.getdados.engine.processors.MapProcessor">
      <property name="extractPath" value="json"/>
    </bean>    
    
    <bean id="pgObjectProcessor" class="com.solutioin.getdados.engine.processors.PgObjectProcessor"/>
    
    <camelContext xmlns="http://camel.apache.org/schema/spring">
      <dataFormats>
         <json id="jack" library="Jackson" />
      </dataFormats>
		<route>
			<from uri="dbmapper:selectMensagem?dataSource=dataSourceGetDados&amp;statementType=SelectList&amp;delay=5000&amp;consumer.onConsume=consumeMensagem" />
			<marshal ref="jack" />
			<to uri="log:com.solutioin.getDados?level=INFO"/>
			<to uri="jms-ex://getDados.envio.{{getDados.fila}}" />
		</route>
    </camelContext>
</beans>
```

A título de informação foi colocado o código completo a ser utilizado com o Camel. Vamos explicá-lo por partes.

No trecho:

```xml
   <bean id="dataSourceGetDados" class="org.springframework.jdbc.datasource.DriverManagerDataSource">
      <property name="driverClassName" value="${getDados.database.driver}" />
      <property name="url" value="${getDados.database.url}" />
      <property name="username" value="${getDados.database.user}" />
      <property name="password" value="${getDados.database.pass}" />
   </bean>

```

é configurado o datasource para conexão com o banco de dados. Note que este datasource foi chamado de **dataSourceGetDados** e assim deverá ser referenciado no restante do código.

No trecho:

```xml
    <bean id="jms-ex"
        class="org.apache.activemq.camel.component.ActiveMQComponent">
        <property name="connectionFactory">
            <bean class="org.apache.activemq.ActiveMQConnectionFactory">
                <property name="brokerURL" value="${getDados.queue.url}" />
            </bean>
        </property>
    </bean>

```
é configurado a url de comunicação com uma mensageria. Note que esta comunicação com mensageria foi chamada de **jms-ex** e assim deverá ser referenciada no restante do código.


Por fim, no trecho:

```xml
		<route>
			<from uri="dbmapper:selectMensagem?dataSource=dataSourceGetDados&amp;statementType=SelectList&amp;delay=5000&amp;consumer.onConsume=consumeMensagem" />
			<marshal ref="jack" />
			<to uri="log:com.solutioin.getDados?level=INFO"/>
			<to uri="jms-ex://getDados.envio.{{getDados.fila}}" />
		</route>

```

Foi implementada a rota de comunicação. Observe que a rota começa com um **from**. Este from acessa o SQL **selectMensagem** do  **mapper** construído anteriormente. Lembre-se que no nosso exemplo, este SQL retorna mensagens não lidas. Observe também, ainda na tag **from** a referência ao datasource **dataSourceGetDados**. No final da tag from temos o atributo *consumer.onConsume=consumeMensagem*. Lembre-se que **consumeMensagem** é o SQL *update* criado anteriormente para marcar a mensagem como consumida. Este SQL será executado no final de toda a execução da ROTA.

A linha subsequente: *marshal ref="jack"*, funciona no sentido converter o *Map* retornado pelo MyBatis em *JSON*.

Desta forma, neste momento teremos um json seguindo o formato:

```json
{"id": 2, "descricao": "mensagem de teste" , "dataMsg": "2019-01-01"}
```

A rota encerra com as tags **to**. Neste exemplo, o JSON gerado será enviado para dois lugares. O primeiro, indicado por **to uri="log:...** é o sistema de log, que no caso, pode ser o console. O segundo, indicado por **to uri="jms-ex://getDados.envio...** é a mensageria com nome **jms-ex** configurada anteriormente. O restante do parâmetro é o nome da fila na qual a mensagem será escrita.


## Formas de execução do GetDados

O GetDados pode ser executado das seguintes formas:

1. Agente Windows: O **GetDados** é distribuido como agente Windows. Quando instalado ele se configura como serviço e pode ser iniciado/parado em **services.msc**.
1. Agente Linux: Existem também um agente.
1. Docker
2. Docker Compose

### A seguir, o exemplo do comando docker:

```bash
docker run -d --name getdados -e KHALA_NAME='NomeDoAgente' -e KHALA.USERNAME='usuario' -e KHALA.PASSWORD='senha' -e prop.database.host='ip do banco' -e prop.database.port='porta do banco' -e prop.database.user='user do banco' -e prop.database.pass='password do banco' solutioin/getdados
```

### Para o docker compose:

Crie um diretório chamado **getdados** e nele um arquivo chamado **docker-compose.yml** com o seguinte conteúdo:

```yaml
version: "3"
services:
  getdados:
    image: eextrato/getdados
    environment:
      - KHALA_NAME=nome da instancia
      - KHALA_USERNAME=usuario da instancia
      - KHALA_PASSWORD=senha da instancia
      - PROP_DATABASE_HOST=ip do banco
      - PROP_DATABASE_PORT=porta do banco
      - PROP_DATABASE_USER=user do banco
      - PROP_DATABASE_PASS=password do banco


```

Dentro do diretório **getdados** execute:

```bash
docker-compose up -d
```

Para atualizar o getdados, execute (ainda dentro do diretório **getdados**)

```bash
docker-compose down
docker-compose pull
docker-compose up -d
```

Para reiniciar o getdados, execute (ainda dentro do diretório **getdados**)

```bash
docker-compose down
docker-compose up -d
```
